import {
  Component,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';
import { DatePipe } from '@angular/common';
import {
  CalendarCell,
  CalendarCellClickEvent,
} from './calendar-cell';
import { cellDimensions } from '../../config';

@Component({
  selector: 'nx-calendar-cell',
  templateUrl: './calendar-cell.component.html',
  styleUrls: ['./calendar-cell.component.css']
})
export class CalendarCellComponent {
  cellWidth: number = cellDimensions.cellWidth;
  cellHeight: number = cellDimensions.cellHeight;

  @Input()
  cell: CalendarCell;

  @Output()
  onClick: EventEmitter<CalendarCellClickEvent> = new EventEmitter<CalendarCellClickEvent>();

  constructor() { }

  @HostListener('click')
  click() {
    this.onClick.emit({
      cell: this.cell
    });
  }

}
