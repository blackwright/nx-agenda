import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';

// ref is a reference to the original item
interface InternalItem {
  ref: any;
  rating: number;
}

@Component({
  selector: 'nx-rateable',
  templateUrl: './rateable.component.html',
  styleUrls: ['./rateable.component.css']
})
export class RateableComponent implements OnChanges {
  @Input()
  items: any[] = [];

  @Input()
  orderBy: string;

  @Input()
  min: number = 0;

  @Input()
  max: number = 1;

  private internalItems: InternalItem[] = [];

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.items) {
      this.mapInternalItems(changes.items.currentValue);
      this.reorderItems();
    }
  }

  mapInternalItems(items: any[]): void {
    this.internalItems = items.map((item) => (
      {
        ref: item,
        rating: 0
      }
    ));
  }

  upvote(item: InternalItem): void {
    if (this.max == null || item.rating < this.max) {
      item.rating += 1;
      this.reorderItems();
    }
  }

  downvote(item: InternalItem): void {
    if (this.min == null || item.rating > this.min) {
      item.rating -= 1;
      this.reorderItems();
    }
  }

  reorderItems(): void {
    const itemsCopy = [ ...this.internalItems ];

    itemsCopy.sort((a, b) => {
      if (a.rating > b.rating) {
        return -1;
      } else if (a.rating < b.rating) {
        return 1;
      } else {
        // ratings are equal, sort by orderBy property on the ref
        if (this.orderBy) {
          const propertyName = this.orderBy;
          if (a.ref[propertyName] < b.ref[propertyName]) {
            return -1;
          } else if (a.ref[propertyName] > b.ref[propertyName]) {
            return 1;
          }
        }
      }
    });

    this.internalItems = itemsCopy;
  }
}
