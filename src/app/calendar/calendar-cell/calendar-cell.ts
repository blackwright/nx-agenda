export interface CalendarCell {
  startDate: Date;
  endDate: Date;
}

export interface CalendarCellClickEvent {
  cell: CalendarCell;
}
