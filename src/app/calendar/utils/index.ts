import {
  startOfWeek,
  addDays,
  addSeconds,
  getDay,
  format,
} from 'date-fns';

export function getUTCDate(localDate: Date = new Date()) {
  return new Date(
    localDate.getUTCFullYear(),
    localDate.getUTCMonth(),
    localDate.getUTCDate(),
    localDate.getUTCHours(),
    localDate.getUTCMinutes(),
    localDate.getUTCSeconds()
  );
}

export function mapWeekToSegmentsPerDay(segmentCount: number, date: Date) {
  const startingDate = startOfWeek(date);
  const weekSegments = [];
  const secondsPerSegment = 86400 / segmentCount;
  for (let day = 0; day < 7; day++) {
    const segmentsPerDay = [];
    for (let segment = 0; segment < segmentCount; segment++) {
      segmentsPerDay.push({
        startDate: addDays(addSeconds(startingDate, secondsPerSegment * segment), day),
        endDate: addDays(addSeconds(startingDate, secondsPerSegment * (segment + 1)), day)
      });
    }
    weekSegments.push(segmentsPerDay);
  }
  return weekSegments;
}

export function mapDayNamesForWeek(date: Date): string[] {
  const startingDate = startOfWeek(date);
  const datesForDays = [];
  for (let day = 0; day < 7; day++) {
    datesForDays.push(addDays(startingDate, day));
  }
  return datesForDays.map(date => format(date, 'dddd'));
}
