import { Component } from '@angular/core';
import { CalendarEvent } from './calendar/calendar-event/calendar-event';
import {
  subDays,
  subHours,
  addHours,
  addMinutes,
  startOfWeek,
} from 'date-fns';

const now = new Date();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  things = [
    { id: 1, name: 'Sruthi' },
    { id: 2, name: 'Matthew' },
    { id: 3, name: 'Jerry' },
    { id: 4, name: 'Rameez' },
  ];

  events: CalendarEvent[] = [
    {
      id: 1,
      editable: false,
      resizable: false,
      startDate: subHours(now, 1),
      endDate: now
    },
    {
      id: 2,
      editable: false,
      resizable: false,
      startDate: subDays(subHours(now, 2), 1),
      endDate: subDays(subHours(now, 1), 1)
    },
    {
      id: 3,
      editable: false,
      resizable: false,
      startDate: addHours(startOfWeek(now), 2),
      endDate: addMinutes(addHours(startOfWeek(now), 2), 30)
    }
  ];
}
