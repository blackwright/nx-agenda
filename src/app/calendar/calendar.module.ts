import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarComponent } from './calendar.component';
import { CalendarCellComponent } from './calendar-cell/calendar-cell.component';
import { CalendarEventContainerComponent } from './calendar-event/calendar-event-container.component';
import { CalendarEventComponent } from './calendar-event/calendar-event.component';

import { CalendarService } from './calendar.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    CalendarComponent,
    CalendarCellComponent,
    CalendarEventContainerComponent,
    CalendarEventComponent,
  ],
  providers: [
    CalendarService,
  ],
  exports: [
    CalendarComponent,
  ]
})
export class CalendarModule { }
