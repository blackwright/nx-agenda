import {
  Component,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { startOfWeek, endOfWeek } from 'date-fns';
import {
  getUTCDate,
  mapWeekToSegmentsPerDay,
  mapDayNamesForWeek,
} from './utils';
import { CalendarCell, CalendarCellClickEvent } from './calendar-cell/calendar-cell';
import { CalendarEvent } from './calendar-event/calendar-event';
import { CalendarService } from './calendar.service';
import { cellDimensions, segmentsPerDay } from '../config';

@Component({
  selector: 'nx-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  headerWidth: number = cellDimensions.cellWidth;

  @Input()
  startingDate: Date = new Date();

  @Input()
  events: CalendarEvent[] = [];

  dayNamesForWeek: string[] = [];
  cells: CalendarCell[] = [];

  @Output()
  onAddEvent: EventEmitter<CalendarEvent> = new EventEmitter<CalendarEvent>();

  @Output()
  onEditEvent: EventEmitter<CalendarEvent> = new EventEmitter<CalendarEvent>();

  constructor(
    private calendarService: CalendarService
  ) { }

  ngOnInit() {
    this.cells = mapWeekToSegmentsPerDay(segmentsPerDay, this.startingDate);
    this.dayNamesForWeek = mapDayNamesForWeek(this.startingDate);

    this.calendarService.calendarStartDate = startOfWeek(this.startingDate);
    this.calendarService.calendarEndDate = endOfWeek(this.startingDate);
  }

}
