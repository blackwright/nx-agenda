export const cellDimensions = {
  cellWidth: 150,
  cellHeight: 25,
};

export const segmentsPerDay = 48;
