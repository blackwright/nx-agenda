import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CalendarEvent } from './calendar-event';
import { getDay } from 'date-fns';

@Component({
  selector: 'nx-calendar-event-container',
  templateUrl: './calendar-event-container.component.html',
  styleUrls: ['./calendar-event-container.component.css']
})
export class CalendarEventContainerComponent implements OnChanges {
  @Input()
  events: CalendarEvent[] = [];

  eventsByDay: CalendarEvent[][] = Array.from(Array(7).keys()).map(el => []);

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.events) {
      this.organizeEvents(changes.events.currentValue);
    }
  }

  organizeEvents(events: CalendarEvent[]): void {
    const organizedEvents: CalendarEvent[][] = Array.from(Array(7).keys()).map(el => []);
    events.forEach((event: CalendarEvent) => {
      const dayIndex = getDay(event.startDate);
      organizedEvents[dayIndex].push(event);
    });
    this.eventsByDay = organizedEvents;
  }

}
