import { Component, Input, OnInit } from '@angular/core';
import { CalendarEvent } from './calendar-event';
import { differenceInMinutes, getDay, getHours } from 'date-fns';

import { CalendarService } from '../calendar.service';
import { cellDimensions, segmentsPerDay } from '../../config';

const { cellWidth, cellHeight } = cellDimensions;

@Component({
  selector: 'nx-calendar-event',
  templateUrl: './calendar-event.component.html',
  styleUrls: ['./calendar-event.component.css']
})
export class CalendarEventComponent {
  @Input()
  event: CalendarEvent;

  @Input()
  index: number = 0;

  @Input()
  totalEvents: number = 0;

  constructor(
    private calendarService: CalendarService
  ) { }

  getEventWidth(): number {
    return cellWidth;
  }

  getEventHeight(): number {
    const minutes = differenceInMinutes(this.event.endDate, this.event.startDate);
    return Math.floor(minutes / 30) * cellHeight;
  }

  getMarginTop(): number {
    const calendarStartingHour = getHours(this.calendarService.calendarStartDate);
    const eventStartingHour = getHours(this.event.startDate);
    const hourDifference = eventStartingHour - calendarStartingHour;
    return hourDifference * cellHeight * segmentsPerDay / 24;
  }

  getMarginLeft(): number {
    return getDay(this.event.startDate) * cellWidth;
  }

}
