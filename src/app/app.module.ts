import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CalendarModule } from './calendar/calendar.module';

import { AppComponent } from './app.component';
import { RateableComponent } from './rateable/rateable.component';

@NgModule({
  declarations: [
    AppComponent,
    RateableComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    CalendarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
