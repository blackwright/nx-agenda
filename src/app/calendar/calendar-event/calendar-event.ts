export interface CalendarEvent {
  id?: number;
  name?: string;
  description?: string;
  editable?: boolean;
  resizable?: boolean;
  startDate?: Date;
  endDate?: Date;
}
