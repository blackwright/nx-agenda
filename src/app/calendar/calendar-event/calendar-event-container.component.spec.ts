import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarEventContainerComponent } from './calendar-event-container.component';

describe('CalendarEventContainerComponent', () => {
  let component: CalendarEventContainerComponent;
  let fixture: ComponentFixture<CalendarEventContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarEventContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarEventContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
