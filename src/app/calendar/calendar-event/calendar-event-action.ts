export interface CalendarEventAction {
  type: ActionTypeEnum;
  startDate: Date;
  endDate: Date;
}

export enum ActionTypeEnum {
  Move,
  Resize,
  Edit
}
